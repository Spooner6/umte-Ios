 //
//  WorkoutViewController.swift
//  MyWorkoutDIary
//
//  Created by Radek Letacek on 19.06.18.
//  Copyright © 2018 Radek Letacek. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
class CustomTableViewCell: UITableViewCell{
    
    @IBOutlet weak var todoLabel: UILabel!
}

class WorkoutViewController: UIViewController, UITableViewDataSource  {
    var data = [String]()
    var ref : DatabaseReference!
    //CalisthenicsTableView
    @IBOutlet weak var tableView: UITableView!
    /// MySkillTableView
    @IBOutlet weak var mySkillTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: "CustomTableViewCell", bundle: nil),forCellReuseIdentifier: "custom")
        ref = Database.database().reference()
        
        let userID = Auth.auth().currentUser?.uid
        ref.child("data").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            guard let value = snapshot.value as? NSDictionary else{
                print("No users data")
                return
            }
            let todoItems = value["todoItem"] as?  [String]
            self.data = todoItems!
            
            
            self.tableView.reloadData()

            
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addItem(_ sender: Any) {
        let alertController = UIAlertController(title: "Přidání do seznamu", message: "Prosím, přidejte položku do plánu", preferredStyle: UIAlertControllerStyle.alert)
        let enterAction = UIAlertAction(title:  "Enter", style: .default, handler: {(_) in
            if let field = alertController.textFields![0] as? UITextField {
                self.data.append(field.text!)
                self.ref.child("data").child( (Auth.auth().currentUser?.uid)!).setValue(["CalisthenicsData":self.data])
                self.tableView.reloadData()
            }
            })
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {(textField) in })
        alertController.addTextField(configurationHandler: {(textField) in
            textField.placeholder =  "Calisthenics Data"
        })
        
        alertController.addAction(enterAction)
        alertController.addAction(cancel)
        
        self.present(alertController, animated: true,completion: nil)
    }
    //addItem to MY SKILLS TABLE VIEW
    @IBAction func addItemToMySkills(_ sender: Any) {
        ////////////
 /*
        let alertController = UIAlertController(title: "Přidání do seznamu", message: "Prosím, přidejte položku do plánu", preferredStyle: UIAlertControllerStyle.alert)
        let enterAction = UIAlertAction(title:  "Enter", style: .default, handler: {(_) in
            if let field = alertController.textFields![0] as? UITextField {
                self.data.append(field.text!)
                self.ref.child("data").child( (Auth.auth().currentUser?.uid)!).setValue(["MySkills":self.data])
                self.tableView.reloadData()
            }
        })
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {(textField) in })
        alertController.addTextField(configurationHandler: {(textField) in
            textField.placeholder =  "My skills data"
        })
        
        alertController.addAction(enterAction)
        alertController.addAction(cancel)
        
        self.present(alertController, animated: true,completion: nil)
        */
        ////////////
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CustomTableViewCell = tableView.dequeueReusableCell(withIdentifier: "custom") as!
        CustomTableViewCell
        cell.todoLabel.text = data[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == UITableViewCellEditingStyle.delete){
            data.remove(at: indexPath.row) //vymaze konkretni radek
            tableView.reloadData() //aktualizuje tableView
            self.ref.child("data").child( (Auth.auth().currentUser?.uid)!).setValue(["calisthenicsData":self.data])
        }
    }
    // @IBAction func addItem(_ sender: Any) {
    //}
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
