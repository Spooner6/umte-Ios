//
//  MySkillsViewController.swift
//  to-do list
//
//  Created by Radek Letacek on 23.06.18.
//  Copyright © 2018 Radek Letacek. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
class SkillTableViewCell: UITableViewCell{
    
    @IBOutlet weak var todoLabel: UILabel!
}

class MySkillsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
