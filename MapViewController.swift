    //
    //  MapViewController.swift
    //  to-do list
    //
    //  Created by Radek Letacek on 07.04.18.
    //  Copyright © 2018 Radek Letacek. All rights reserved.
    //

    import UIKit
    import MapKit

    class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

        @IBOutlet weak var addressLabel: UILabel!
        @IBOutlet weak var mapView: MKMapView!
        @IBOutlet weak var distanceLabel: UILabel!
        
        var locationManager = CLLocationManager()
        var userLocation = CLLocation()
        var finalAddress = " "
        var finalLocation : CLLocation! = nil
        var finalDistance : CLLocationDistance = 0.0
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            locateMe()

            // Do any additional setup after loading the view.
        }

        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        func locateMe(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            
            mapView.showsUserLocation = true
        }
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            var userLocation = locations[0] as CLLocation
            //zoom na usera
            //locationManager.stopUpdatingLocation()
            //vycentroavni mapy na usera
            let coordinates = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
            //velikost zoomu
            let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
            let region = MKCoordinateRegion(center: coordinates, span: span)
            mapView.setRegion(region, animated: true)
            
            findNearest()
        }
        
        func findNearest(){
            let request = MKLocalSearchRequest()
            request.naturalLanguageQuery = "gym"
            request.region = mapView.region
            
            let search = MKLocalSearch(request: request)
            
            search.start(completionHandler: {(response, error) in
                if error != nil{
                print("ERROR ")
                }else if response!.mapItems.count == 0{
                print ("No matches found")
                }else{
                print("Matches found")
                    
                for place in response!.mapItems{
                    //lokalita.. adresa, atd.
                    //print(place.placemark.subThoroughfare! + " " + place.placemark.thoroughfare! + " " +
                    //place.placemark.locality! + " " + place.placemark.administrativeArea!)
                    
                    
                    let placemark = CLLocation(latitude: place.placemark.coordinate.latitude,
                                               longitude: place.placemark.coordinate.longitude)
                
                    if self.finalLocation == nil{
                        self.finalLocation = placemark
                        self.finalDistance = self.userLocation.distance(from: placemark)
                        self.finalAddress = place.placemark.subThoroughfare! + " " + place.placemark.thoroughfare! + " " +
                            place.placemark.locality! + " " + place.placemark.administrativeArea!
                        
                    }else{
                        if self.finalDistance > self.userLocation.distance(from: placemark){
                            self.finalLocation = placemark
                            self.finalDistance = self.userLocation.distance(from: placemark)
                            self.finalAddress = place.placemark.subThoroughfare! + " " + place.placemark.thoroughfare! + " " +
                                place.placemark.locality! + " " + place.placemark.administrativeArea!
                        }
                    }
                }
                    self.addAnnotation()
                    self.updateView()
                    
                }
            
        })
        }
        func addAnnotation(){
            let anno = MKPointAnnotation()
         anno.coordinate = self.finalLocation.coordinate
            anno.title = "gym"
            mapView.addAnnotation(anno)
        }
        func updateView(){
            addressLabel.text = finalAddress
            distanceLabel.text = "\(Int(finalDistance)) meters"
        }

        @IBAction func openInMaps(_ sender: Any) {
            let coords = finalLocation.coordinate
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coords))
            mapItem.name = "gym"
            mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
        }
        /*
        // MARK: - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            // Get the new view controller using segue.destinationViewController.
            // Pass the selected object to the new view controller.
        }
        */

    }
