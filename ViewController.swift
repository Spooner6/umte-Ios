//
//  ViewController.swift
//  to-do list
//
//  Created by Radek Letacek on 06.04.18.
//  Copyright © 2018 Radek Letacek. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    //v menu skryje horni lista
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    //u mapy je zobrazen horni panel s tlacitkem back
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    @IBAction func findPlace(_ sender: Any) {
        //tlacitko pro najiti nejblizsiho mista na cviceni
        //odkaz, diky kterememu se zavola spoj maps .. self je pro zobrazeni sama sebe
        performSegue(withIdentifier: "maps", sender: self)
        
    }
    @IBAction func myExercises(_ sender: Any) {
        //tlacitko pro zobrazeni seznamu cviku
        
    }
    @IBAction func exercisePlans(_ sender: Any) {
        //tlacitko pro zobrazeni treninkoveho planu
        
    }
    
    @IBAction func logOut(_ sender: Any) {
        do{
            try Auth.auth().signOut()
            performSegue(withIdentifier: "back", sender: self)
        }catch{
            print("No user is lgged in")
        }
    }
    

}

